from flask import Flask, request

app = Flask(__name__)

employee_database = dict([(i, 'Aravind K (%s)' % i) for i in range(1, 20)])


@app.route('/employee', methods=['GET', 'POST'])
def getEmployeeName():
    if request.method =='POST':
        employeeID = request.form.get('employeeID')
    else:
        employeeID = request.args.get('employeeID')
    employeeID = int(employeeID)
    if employeeID in employee_database:
        return employee_database.get(employeeID)
    else:
        return 'Employee with ID %s doesnt exist.' % employeeID


if __name__ == '__main__':
    app.run()
