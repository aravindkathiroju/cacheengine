import requests
from flask import Flask, request

app = Flask(__name__)


class InMemCursor(object):
    def __init__(self, employee_name):
        self.employee_name = employee_name

    def fetchall(self):
        return self.employee_name


class InMemDB(object):
    def __init__(self):
        self.cache_employee_database = dict([(i, 'Aravind K (%s)' % i) for i in range(1, 6)])
        self.employeeID = None

    def connect(self):
        return self

    def execute(self, employeeID):
        self.employeeID = employeeID
        return InMemCursor(self.cache_employee_database.get(self.employeeID))


def retrieveEmployeeFromCache(employeeID):
    inMemDB = InMemDB()
    inMemDBObj = inMemDB.connect()
    return inMemDBObj.execute(employeeID).fetchall()


MAIN_URL = 'http://127.0.0.1:5000/employee'


def retrieveEmployeeFromMainSource(employeeID):
    response = requests.post(MAIN_URL, data={'employeeID': employeeID})
    if response.status_code == requests.codes.ok:
        return response.content
    else:
        return 'Employee with ID %s doesnt exist' % employeeID


@app.route('/cache/employee/')
def getEmployeeName():
    employeeIDs = request.args.get('employeeIDs')
    employeeIDs = employeeIDs.split(',')
    employee_records = {}
    for emp_id in employeeIDs:
        employeeID = int(emp_id)

        e_name = retrieveEmployeeFromCache(employeeID)

        if not e_name:
            e_name = retrieveEmployeeFromMainSource(employeeID)

        employee_records[employeeID] = str(e_name)
    return employee_records


if __name__ == '__main__':
    app.run(port=5001)
