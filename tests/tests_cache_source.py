from mock import patch
import sys
import json
import unittest
import cache_source


class CacheSourceTest(unittest.TestCase):

    def setUp(self):
        self.app = cache_source.app.test_client()

    def test_valid_query(self):
        res = self.app.get('/cache/employee/?employeeIDs=3,4')
        # data is returned from cache_source implementation
        self.assertEqual(
            json.loads(res.get_data().decode(sys.getdefaultencoding())),
            {u'3': u'Aravind K (3)', u'4': u'Aravind K (4)'}
        )

    def test_invalid_query(self):
        res = self.app.get('/cache/employee/?employeeIDs=23,15')
        # hits main_source (another rester service) and gives you data
        self.assertEqual(
            json.loads(res.get_data().decode(sys.getdefaultencoding())),
            {u'15': u'Aravind K (15)', u'23': u'Employee with ID 23 doesnt exist.'}
        )

    def getArgs(self, emp_id):
        return '5'

    @patch('cache_source.request')
    def test_modified_query_using_mock_on_flask_request(self, mockedRequest):
        mockedRequest.args.get.side_effect = self.getArgs
        res = self.app.get('/cache/employee/?employeeIDs=23')  # though we query for 23 rec (which is not existing in
        # cache), mock modified it to look for 5. 5 exists in cache_source.
        self.assertEqual(
            json.loads(res.get_data().decode(sys.getdefaultencoding())),
            {u'5': u'Aravind K (5)'}
        )

    def myPostAction(self, *args, **kwargs):
        class MyResponse(object):
            def __init__(self, status_code, content):
                self.status_code = status_code
                self.content = content

        return MyResponse(200, 'Hitting Main Source')

    @patch('cache_source.requests')
    def test_invalid_query_using_mock_requests_post(self, mockedRequests):
        mockedRequests.post.side_effect = self.myPostAction
        mockedRequests.codes.ok = 200
        res = self.app.get('/cache/employee/?employeeIDs=23')  # 23 rec doesnt exist in cache, it hits main source
        # using requests.post, using mock, we intercepted post call and provided our own response.
        self.assertEqual(
            json.loads(res.get_data().decode(sys.getdefaultencoding())),
            {u'23': u'Hitting Main Source'}
        )


if __name__ == "__main__":
    unittest.main()
